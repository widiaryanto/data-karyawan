//
//  DetailController.swift
//  Data Pegawai
//
//  Created by Muhamad Widi Aryanto on 08/09/19.
//  Copyright © 2019 Widi Aryanto. All rights reserved.
//

import UIKit
import CoreData

class DetailController: UIViewController {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var birthDate: UILabel!
    
    var employeesID = 0
    // Declaration Core Data from AppDelegate.swift
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        let editButton = UIBarButtonItem(image: UIImage(named: "edit"), style: .plain, target: self, action: #selector(edit))
        let deleteButton = UIBarButtonItem(image: UIImage(named: "delete"), style: .plain, target: self, action: #selector(deleteAction))
        self.navigationItem.rightBarButtonItems = [editButton, deleteButton]
    }
    
    @objc func edit() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "addFormEmployeesController") as! AddFormEmployeesController
        controller.employeeID = employeesID
        self.navigationController?.pushViewController(controller, animated: true)
    }

    @objc func deleteAction() {
        let alertController = UIAlertController(title: "Warning", message: "Are you sure to delete this item?", preferredStyle: .actionSheet)
        let alertActionYes = UIAlertAction(title: "Yes", style: .default) { (action) in
            let employeeFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Employees")
            employeeFetch.fetchLimit = 1
            // Condition with predicate
            employeeFetch.predicate = NSPredicate(format: "id == \(self.employeesID)")
            // Run
            let result = try! self.context.fetch(employeeFetch)
            let employeesToDelete = result.first as! NSManagedObject
            self.context.delete(employeesToDelete)
            
            do {
                try self.context.save()
            } catch {
                print(error.localizedDescription)
            }
            self.navigationController?.popViewController(animated: true)
        }
        let alertActionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(alertActionYes)
        alertController.addAction(alertActionCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let employeeFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Employees")
        employeeFetch.fetchLimit = 1
        // Condition with predicate
        employeeFetch.predicate = NSPredicate(format: "id == \(employeesID)")
        // Run
        let result = try! context.fetch(employeeFetch)
        let employees: Employees = result.first as! Employees
        
        firstName.text = employees.firstName
        lastName.text = employees.lastName
        email.text = employees.email
        birthDate.text = employees.birthDate
        
        if let imageData = employees.image {
            imageDetail.image = UIImage(data: imageData as Data)
            imageDetail.layer.cornerRadius = imageDetail.frame.height / 2
            imageDetail.clipsToBounds = true
        }
        self.navigationItem.title = "\(employees.firstName!) \(employees.lastName!)"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
